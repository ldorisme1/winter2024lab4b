import java.util.Scanner;	

public class VirtualPetApp {
	public static void main(String[] arg){
		Scanner reader = new Scanner(System.in);
		Swan[] flock = new Swan[2];
		
		for(int i=0; i < flock.length; i++){
			
			System.out.println("Is the swan " + i + " flying?");
			boolean userbool = Boolean.parseBoolean(reader.nextLine());
			
			System.out.println("What color is swan " + i + "?");
			String itsColor = reader.nextLine();

			System.out.println("How many insects has swan " + i + " eaten?");
			int insectnum = Integer.parseInt(reader.nextLine());
			// here iam take the user input to my constructor method
			flock[i] = new Swan(userbool, itsColor, insectnum);
		}
		
		System.out.println(flock[flock.length-1]. getIsFlying());
		System.out.println(flock[flock.length-1]. getBodyColor());
		System.out.println(flock[flock.length-1].getInsectsEaten());
		System.out.println("Please update the number of insect eaten by the last animal ");
		int updateInsect = Integer.parseInt(reader.nextLine());
		flock[flock.length-1].setInsectEaten(updateInsect);
		System.out.println(flock[flock.length-1]. getIsFlying());
		System.out.println(flock[flock.length-1]. getBodyColor());
		System.out.println(flock[flock.length-1].getInsectsEaten());
		// we will be able to see the difference after updating the field with the set method created 
	
	}
}