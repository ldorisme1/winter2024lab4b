public class Swan {
	private boolean isFlying;
	private String bodyColor;
	private int insectsEaten;
	
	// can't use the constructor tu update fields
	public Swan(boolean theflying,  String thebodycolor, int theInsectEaten){
		this.isFlying = theflying;
		this.bodyColor= thebodycolor;
		this.insectsEaten = theInsectEaten;
	}
	public void flyingSpeed(){
		if(insectsEaten > 10){
			System.out.println("The swan is flying slowly");
		}
		else{
			System.out.println("The swan is speeding across the sky");
		}
	}
	
	public void makesNoise(){
		if(isFlying){
			System.out.println("QUACK!");
		}
		else{
			System.out.println("**silence**");
		}
	}
	public boolean getIsFlying(){
		return this.isFlying;
	}
	
	public String getBodyColor(){
		return this.bodyColor;
	}
	
	public int getInsectsEaten(){
		return this.insectsEaten;
	}
	
	// only set method that we decided to leave to be ablue tu update it later
	public void setInsectEaten(int newInsectEaten){
		this.insectsEaten= newInsectEaten;
	}
	
}